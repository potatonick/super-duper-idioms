# Super Duper Idioms
A collection of english idioms alongside their spanish equivalents plus examples.

## Links
http://www.idiomconnection.com/

https://www.learn-english-today.com/idioms/idioms_proverbs.html

http://www.saberingles.com.ar/idioms/


## Colors

### White

- **White coffee ➔** Café con leche

- **White lie ➔** Mentira piadosa

- **A white elephant ➔** _Posesión que es inútil y a menudo costosa de mantener (como un ferrary)_

### Green

- **The grass is always greener on the other side ➔** _Es un refrán que tiene el significado de que siempre nos parecen mejores las cosas de los demás que las nuestras. En español podríamos tener algún equivalente, como **vacas ajenas dan mejor carne**_

- **To be green ➔** Estar verde, ser inexperto, no estar maduro

- **To be green with envy ➔** Estar verde de envidia


### Blue (my favourite idioms 🙃)

- **Being blue ➔** Es estar melancólico, pero también, estar morado del frío

- **Out of the blue ➔** Algo que ocurre inesperadamente, sin avisar, de la nada

- **A bolt from the blue ➔** Algo inesperado, pero malo (como un exámen sorpresa)

- **To go off into the blue ➔** Irse derrepente sin dejar rastro

- ~~**Blue jeans ➔** Pantalones vaqueros~~

- **Blue joke ➔** Es el equivalente, pero con otro color, a lo que nosotros llamamos **(chiste verde)**

- **Once in a blue moon ➔** _Expresión que se utiliza para decir que algo ocurre cada mucho tiempo. Un equivalente podría ser **de higos a brevas**_

- **Talk a blue streak ➔** Es la frase hecha equivalente a **hablar por los codos**

- **To look/feel blue ➔** Estar deprimido


### Pink

- **Be in the pink ➔** Estar de maravilla

- ~~**A pink slip ➔** Es una carta de despido~~


### Red

- **As red as beetroot ➔** Rojo como un tomate

- **To catch someone red-handed ➔** Pillar a alguien con las manos en la masa

- **To be in the red ➔** Estar en números rojos

- **A red-letter day ➔** Un día señalado

- **To paint the town red ➔** Irse de juerga, to go out to bars or clubs to enjoy yourself.

- **To see red ➔** Volverse furioso

- **A red herring ➔** Unimportant matter introduced into a discussion to divert attention from the main subject (rojo como un arenque)


### Purple

- **Purple ➔** _Algo que es muy florido, por ejemplo, se puede decir **purple prose** para referirse a una prosa muy ornamentada, muy cargada_


### Brown

- ~~**To be browned off ➔** Estar aburrido~~

- **Make brown ➔** Broncearse

- **As brown as a berry ➔** Estar moreno

- ~~**To brown (verb) ➔** _Relacionado con comida, significa dorar, sofreír, saltear_~~

- ~~**Brown-nose ➔** Es una persona pelota~~

- **In a brown study ➔** Pensamiento profundo


### Black

- **To be in the black ➔** Lo contrario de **to be in the red**

- **To black out ➔** Perder la consciencia, desmayarse

- **To blackmail ➔** Chantajear a alguien

- **Pitch black ➔** _Se utiliza esta expresión para decir que algo es muy muy oscuro, o como diríamos en español, **negro como el carbón**_

- **To be black and blue ➔** Estar lleno de moratones

- **A black eye ➔** Mala reputación

- **A black day ➔** Un día horrible porque algo malo pasó

- ~~**A blacklist ➔** Lista negra~~

- ~~**A black look ➔** Mirada de envidia/enojo~~

- ~~**Black market ➔** Mercado negro~~

- **In black and white ➔** Por escrito

- **The black sheep ➔** La oveja negra

- **The pot calling the kettle black ➔** Ver paja en ojo ajeno y no ver viga en el propio


### Gold

- **Silence is golden ➔** En boca cerrada no entran moscas

- **All that glitters is not gold ➔** Las apariencias engañan


### Silver

- **Every cloud has a silver lining ➔** Siempre hay un lado positivo


### Yellow

- **The yellow press ➔** Titulares llamativos/sensacionalistas


## Weather

 - **On cloud nine ➔** En el séptimo cielo

 - **Come rain or shine ➔** Pase lo que pase

 - **Storm in a teacup ➔** Una montaña de un grano de arena

 - **Face (look/be) like thunder ➔** Estar muy muy enfadado

 - **To fell under the weather ➔** Sentirse enfermo

 - **To weather the storm ➔** Pasar una mala experiencia y no haber sufrido mucho (como una pelicula de terror)

 - **To dance/sing up a storm ➔** _Hacer algo con energía (no hace falta que sean esos dos verbos)_

 - **To have one's head in the clouds ➔** Divagar

 - **To be under a cloud (of suspicion) ➔** _Ser sospechoso de hacer algo ilegal_

 - **A cloud on the horizon ➔** _Something that might spoil a happy situation_

 - **Every cloud has a silver lining ➔** _There is something good even in a difficult or sad situation_

 - **In floods of tears ➔** Llorar como una magdalena

 - **To shower somebody with something ➔** _To give somebody a lot of something_

 - **To shower something on / over something ➔** Esparcir

 - **To not have the foggiest idea ➔** Pillarte fuera de juego (no tener ni idea)

 - **A frosty (sustantivo) ➔** Un no amigable...

 - **As right as rain ➔** Perfecto



## Fruit

- To bear fruit:(a plan, a decision, etc) to be successful, especially after a long time.

- The fruit of my labour: los frutos de mi trabajo.

- To be the apple of somebody's eyes: ser la niña de los ojos de alguien.

- A rotten apple: a bad person that has a bad effect on the rest of the group.

- Apple polisher: como Brown-nose, una persona pelota.

- To compare apples to oranges: compare things that are completely different.

- The apple doesn't fall far from the tree: used to say that children are usually similar to their parents.

- To upset the apple cart: do something that spoils somebody's plans.

- To go bananas: become very angry or excited.

- A second bite of the cherry: a second chance to do something.

- Sour grapes: something that you really want but you can't have, and so you say that you don't want it.

- A lemon: a silly person.

- A lemon: something useless because it doesn't work properly.

- A real peach: something/somebody that is nice or good.



## Animals

### Bird

- A bird in the hand is worth two in the bush: más vale pájaro en mano que ciento volando.

- The early bird catches the worm: al que madruga dios le ayuda.

- Birds of a feather flock together: Dios los cría y ellos se juntan.

- To kill two birds with one stone: matar dos pájaros de un tiro.

- An early bird: a person who arrives or gets up early.


### Monkey

- Monkey business/tricks: dishonest or bad behaviour.

- Brass monkey weather: very cold weather.

- To make a monkey out of somebody: to make somebody look stupid.


### Pig

- A pig with lipstick, is still a pig: aunque la mona se vista de seda, mona se queda.

- pigs might fly: expression used to say that you do not believe that something will happen.

- To buy a pig in a poke: to buy something that is not as good as you thought, comprar gato por liebre.

- To make a pig of oneself: to eat a lot, comer en gran cantidad, darse un atracón.

- To make a pig's ear of something: to make something very badly, hacer algo mal, como la mona.

- A guinea pig: somebody used in a scientific test, un conejillo de las Indias.

- To pig out: to eat a lot all at once.

- To sweat like a pig: to sweat a lot.

- To make a silk purse out of a sow's ear: to make something good out of something that is bad quality.



### Cat

- All cats are grey in the dark: por la noche todos los gatos son pardos.

- Hunt with cats and you catch only rats: dime con quién andas y te diré lo que  eres.

- A cat has nine lives: un gato tiene siete vidas (en inglés el gato tiene dos vidas más).

- Curiosity killed the cat (satisfaction brought it back): it may be dangerous to be too curious.

- To fight like cat and dog: to fight a lot.

- Has a cat got your tongue?: ¿te comieron la lengua los ratones? Story.

- To let the cat out of the bag: to reveal a secret carelessly.

- Like a cat on hot bricks: very nervous.

- No room to swing a cat: not enough space.

- To have a cat nap: to have a short sleep.

- To play cat and mouse with somebody: to keep somebody in uncertain expectation, treating him alternately cruelly and kindly.

- Then the cat's away, the mice will play: cuando el gato no está, los ratones se divierten.

- To put/set the cat among the pigeons: to introduce somebody/something that is likely to cause trouble Story.

- It's raining cats and dogs: it's raining a lot, está lloviendo a cántaros.

- A cat in gloves catches no mice: gato con guantes no caza ratones.

- Like the cat that got the cream: very pleased or happy about something that you have achieved.

- It's raining cats and dogs: llueve a cantaros.



## Number

### One

- At one time: en el pasado, en un momento dado.

- Back to square one: de nuevo en el comienzo.

- For one thing: principalmente.

- It's all one to me: es todo lo mismo para mí.

- One and the same: exactamente el mismo.

- One fine day: algún día.

- One for the road: una para el camino (cuando se toma una copa más antes de salir).

- One in a thousand: uno en mil.

- One in a million: uno en un millón.

- One of a kind: único en su especie.

- One of these days: uno día de estos, pronto.

- One too many: demasiado.

- The one and only: el único.

- To be at one with somebody: estar de acuerdo con alguien.

- To have one over the eight: tomar mucho alcohol.

- To pull a fast one on somebody: engañar a alguien.

- Don't put all your eggs in one basket: no te lo juegues todo a una carta.

- One man's meat is another man's poison: para gustos los colores.


### Two

- In two shakes of a lamb's tail: rápido, en un periquete, en un abrir y cerrar de ojos.

- In twos and threes: de a dos o tres.

- It takes two to do something: una persona sola no es responsable de un casamiento feliz/infeliz, una pelea, un acuerdo.

- It takes two to tango: es cuestión de dos (Se necesitan dos personas para alcanzar algo).

- To be in two minds about something: estar indeciso sobre algo.

- To be two-faced: no ser sincero.

- To have two bites of the cherry: tener dos chances.

- To have two strings to your bow: tener una segunda alternativa por si la primera falla.

- To put two and two together: deducir, darse cuenta.

- A bird in the hand is worth two in the bush: más vale pájaro en mano que ciento volando.



## Others

- Split the beans: irse de la lengua.

- Through thick and thin: sin importar lo que pase/a cualquier precio.

- It's the best thing since sliced bread: lo mejor de lo mejor (en realidad no hay traducción, sólo se utiliza cuando algo es algo increíble).

- Take it with a pinch of salt: no tomárselo demasiado enserio.

- Go down in flames: fracasar estrepitosamente.

- You can say that again: es cierto (por más que sea una frase de inicio a fin, sólo sirve para hacer saber a los demás que estás de acuerdo con lo que han dicho, pero no lo utilices en una propuesta).

- Break a leg: ánimo/suerte (como fighting en Corea).

- See eye to eye: estar totalmente de acuerdo.

- Jump on the bandwagon: seguir una moda (sí, el verbo se conjuga).

- Beat around the bush: evitar decir algo (cuando no quieres decirlo o contestar con sinceridad).

- Hit the sack: irse a la cama.

- By the skin of your teeth: a duras penas/por los pelos.

- A chip off the old block: de tal palo tal astilla.

- As mad as a hatter: más loco que una cabra.

- Drive me nuts: me vuelve loco.

- To go bananas: volverse loco.

- Out of my main: estar loco.

- Cost an arm and a leg: cuesta un ojo de la cara (allí incluso lo más caro es más costoso).

- Picture paints a thousand words: una imagen vale más que mil palabras.

- Absence makes the heart grow fonder: la ausencia es al amor como el aire al fuego, apaga el pequeño y aviva el grande.

- Ablessing in disguise: no hay mal que por bien no venga.

- Achance is as good as a rest: con un cambio de actividad se renuevan las energías

- A chip on his shoulder: una espina clavada en la espalda (guardar rencor).

- Actions speak louder than words: las palabras se las lleva el viento/Una acción vale más que mil palabras.

- Addfuel to the fire: echar leña al fuego.

- A dime a dozen: los hay a patadas (muchos y baratos).

- A drop in the bucket/ocean: una gota en el océano.

- Against the clock: a toda pastilla/prisa.

- A leopard can't change its spots: quién nace lechón muere gorrino.

- Allbark and no bite: perro ladrador poco mordedor.

- Allin the same boat: estamos todos en el mismo barco (compartimos el mismo destino).

- Allmouth and no trousers: mucho ruido y pocas nueces.

- Allor nothing: todo o nada.

- An eye for an eye and tooth for a tooth/tit for tat: ojo por ojo y diente por diente/donde las dan las toman.

- Andthey lived happily ever after: vivieron felices y comieron perdices.

- A piece of cake: es pan comido.

- Taste of your own medicine: probar de tu propia medicina.

- At the drop of a hat: en menos que canta un gallo.

- To be as easy as pie: ser pan comido.

- A watcher por never boils: quién espera, desespera.

- A stitch in time saves nine/better safe than sorry: más vale prevenir que curar.

- A word is enough to the wise: a buen entendedor pocas palabras bastan.

- All cats are grey in the dark: por la noche todos los gatos son pardos.

- Better late than never: más vale tarde que nunca.

- Better the devil you know that the devil you don't know: más vale malo conocido que bueno por conocer.

- Don't bite more than you can chew: el que mucho aprieta poco abarca.

- Friends, keepers; losers, weepers: quién ser fue a Sevilla perdió su silla.

- In for a penny, in for a pound (from lost to the river): de perdidos al río.

- It's never too late to learn: nunca es tarde para aprender.

- No news is good news: la falta de noticias es una buena noticia.

- The die is cast: la suerte está echada.

- Think twice, act wise: Piénsalo dos veces antes de actuar.

- When there's a will, there's a way: querer es poder.

- You scratch my back, I'll scratch yours: hoy por ti, mañana por mí.

- Take it easy: relajate.

- Everybody aboard the hype train: (es la típica frase que se dice cuando alguien sigue una moda).